package socketController;

import misc.NameGenerator;

import java.io.IOException;
import java.net.*;

public class SocketController {

    private MulticastSocket socket;
    private String id;
    private InetAddress ia;
    private final int port;
    private byte[] mes;

    public SocketController(String ipAddress, int port, int timeout) throws IOException {
        this.port = port;
        ia = InetAddress.getByName(ipAddress);
        socket = new MulticastSocket(port);
        socket.setSoTimeout(timeout);
        if (!ipAddress.equals("255.255.255.255"))
            try {
                socket.joinGroup(ia);
            } catch (SocketException e) {
                System.out.println("No such device.");
                socket.close();
                System.exit(1);
            } catch (IOException e) {
                System.out.println("\033[0;31m" + ipAddress + "\033[0m is not a multicast address.");
                try {
                    socket.leaveGroup(ia);
                } catch (IOException ignored) {
                }
                socket.close();
                System.exit(1);
            }
        else
            socket.setBroadcast(true);

        /*Creating unique ID*/
        mes = NameGenerator.generateName();
        id = new String(mes, 0, 6);

        System.out.println("Internet address to which packet are sent: \033[0;32m" + ia.getCanonicalHostName() + ":" + socket.getLocalPort() + "\033[0m");
    }

    public DatagramPacket listen() throws IOException {
        byte[] buf = new byte[6];
        DatagramPacket packet = new DatagramPacket(buf, buf.length);
        try {
            socket.receive(packet);
        } catch (SocketTimeoutException e) {
            throw new SocketTimeoutException();
        }
        return packet;
    }

    public void write() throws IOException {
        DatagramPacket packetOut = new DatagramPacket(mes, mes.length, ia, port);
        socket.send(packetOut);
    }

    public void close() {
        try {
            socket.leaveGroup(ia);
        } catch (IOException ignored) {
        }
        socket.close();
    }

    public String getId() {
        return id;
    }
}
