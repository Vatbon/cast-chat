package misc;

import java.util.Random;

public class NameGenerator {
    public static byte[] generateName() {
        byte[] bytes = new byte[6];
        Random random = new Random();
        random.setSeed(System.currentTimeMillis());
        random.nextBytes(bytes);
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) (33 + (Math.abs(bytes[i]) % 64));
        }
        return bytes;
    }
}
