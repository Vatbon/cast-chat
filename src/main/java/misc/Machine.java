package misc;

/**
 * This object serves need to store info about other PCs in group or broadcast.
 */
public class Machine {
    private Long lastUpdate = System.currentTimeMillis();
    private final String id;
    private final String ip;

    public Machine(String id, String ip) {
        this.id = id;
        this.ip = ip;
    }

    public void update() {
        lastUpdate = System.currentTimeMillis();
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public String getId() {
        return id;
    }

    public String getIp() {
        return ip;
    }
}
