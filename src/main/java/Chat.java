import misc.Machine;
import socketController.SocketController;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.SocketTimeoutException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Chat {

    private final static int timeout = 3000; // in millisecond
    private final static int timeOfDeath = 10000; // in millisecond

    public static void main(String[] args) {

        String ipAddress;
        int port;
        if (args.length > 1) {
            ipAddress = args[0].trim();
            port = Integer.parseInt(args[1].trim());
        } else {
            System.out.println("No IP address given.");
            return;
        }

        SocketController socketController;
        try {
            socketController = new SocketController(ipAddress, port, timeout);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        String uniqueId = socketController.getId();
        System.out.println("I am \033[0;33m" + uniqueId + "\033[0m");
        Map<String, Machine> machines = new HashMap<>();
        long start;
        while (true) {
            /*Read console to find word 'quit' and exit the program*/
            try {
                if (System.in.available() > 4) {
                    String input = System.console().readLine();
                    if (input != null) {
                        if ("quit".equals(input)) {
                            System.out.println("Exit program.");
                            socketController.close();
                            break;
                        }
                    }
                }
            } catch (IOException ignored) {
            }

            start = System.currentTimeMillis();
            /*Read group or broadcast*/
            try {
                while (System.currentTimeMillis() - start < timeout) {
                    DatagramPacket mes = socketController.listen();
                    String id = new String(mes.getData(), 0, mes.getLength());
                    if (!id.equals(uniqueId)) {
                        if (machines.containsKey(id)) {
                            machines.get(id).update();
                        } else {
                            machines.put(id, new Machine(id, mes.getAddress().getHostAddress()));
                        }
                    }
                }
                /*Show machines and write into group or broadcast*/
                printMachines(machines);
                socketController.write();
                /*If see no one, write into group or broadcast*/
            } catch (SocketTimeoutException e) {
                try {
                    printMachines(machines);
                    socketController.write();
                } catch (IOException ex) {
                    ex.printStackTrace();
                    socketController.close();
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
                socketController.close();
                break;
            }
        }
    }

    private static void printMachines(Map<String, Machine> machines) {
        long instant = System.currentTimeMillis();
        Collection<Machine> machineCollection = machines.values();
        Iterator<Machine> iterator = machineCollection.iterator();

        /*Delete machines, which seem to be "dead"*/
        while (iterator.hasNext()) {
            Machine machine = iterator.next();
            if (instant - machine.getLastUpdate() > timeOfDeath) {
                iterator.remove();
                //machines.remove(machine.getId());
            }
        }

        /*Print alive machines*/
        if (machineCollection.size() == 0)
            System.out.println("I don't see anyone.");
        else {
            System.out.println("I see \033[0;32m" + machineCollection.size() + "\033[0m apps like me:");
            machineCollection = machines.values();
            iterator = machineCollection.iterator();
            while (iterator.hasNext()) {
                Machine machine = iterator.next();
                System.out.println("--Id: " + machine.getId() + " from \033[0;33m" + machine.getIp() + "\033[0m");
            }
        }
    }
}
